<?php
require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare('world', false, false, false, false);

for ($i = 0; $i < 100; $i++) {
	$toChannel = ($i % 2 == 0) ? 'hello' : 'privet';
	$msg = new AMQPMessage('Hello World!::' . sha1(time().$i). " " . $toChannel);
	$channel->basic_publish($msg, '', $toChannel);
}
echo " [x] Sent 'Hello World!'\n";

$channel->close();
$connection->close();