<?php

require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare('privet', false, false, false, false);
$channel->queue_declare('hello', false, false, false, false);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";
$callback = function(\PhpAmqpLib\Message\AMQPMessage $msg) {
	$text =  " [x] Received ". $msg->body.' | '. date('Y-m-d H:i:s')."\n";
	file_put_contents(__DIR__.'/text.log', $text, FILE_APPEND);
};

$channel->basic_consume('hello', '', false, true, false, false, $callback);
$channel->basic_consume('privet', '', false, true, false, false, $callback);

while(count($channel->callbacks)) {
	$channel->wait();
}