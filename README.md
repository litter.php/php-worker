1) Install rabbitMq and arara/process in composer

    # composer install
    
2) Create deamon file in /etc/init.d/phpd-worker
    
---in file phpd-worker:

    #!/usr/bin/php
    ### BEGIN INIT INFO
    # Provides:          phpd-worker
    # Required-Start:    $network $syslog $remote_fs
    # Required-Stop:     $network $syslog $remote_fs
    # Default-Start:     2 3 4 5
    # Default-Stop:      0 1 6
    # Short-Description: php worker daemon
    # Description:       Help to use rabbitMq
    ### END INIT INFO
    <?php
    include '/full_path_to_project/worker/rabbit-wq-receiver.php';

3) To enable phpd-worker as deamon (in Ubuntu) 

    # sudo systemctl enable phpd-worker

Then you can use commands stop, start, status for service phpd-worker


Other :
    
    #sudo apt-get install sysv-rc-conf